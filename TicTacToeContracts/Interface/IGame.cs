﻿using System;
using System.Collections.Generic;
using System.Text;
using TicTacToeContracts.DTO;
using TicTacToeContracts.Interface.Players;

namespace TicTacToeContracts.Interface
{
    
    public interface IGame
    {
        void Start(); 
        IPlayer P1 { get; }
        IPlayer P2 { get;  }
        bool EndGame();
        XO GameResult();
        IBoard Board { get; }
    }
}
