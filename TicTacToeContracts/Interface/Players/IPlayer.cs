﻿using System;
using System.Collections.Generic;
using System.Text;
using TicTacToeContracts.DTO;

namespace TicTacToeContracts.Interface.Players
{
    public interface IPlayer
    {
        public XO XO { get; set; }
        bool MakeMove(IBoard board); 
    }
}
