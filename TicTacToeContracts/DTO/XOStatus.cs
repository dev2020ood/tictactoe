﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace TicTacToeContracts.DTO
{
    public class XOStatus
    {
        public XO XO { get; set; }
        public int Occupied { get; set; }


    }
}
