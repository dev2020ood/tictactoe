﻿
using Contracts;
using System;
using TicTacToeContracts.DTO;
using TicTacToeContracts.Interface;
using TicTacToeContracts.Interface.Players;

namespace XOHumanPlayer
{
    [Register(Policy.Transient,typeof(IHumanPlayer))]
    public class HumanPlayer : IHumanPlayer
    {
        public XO XO { get; set; }

        public bool MakeMove(IBoard board)
        {
            throw new NotImplementedException();
        }
    }
}
