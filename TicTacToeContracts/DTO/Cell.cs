﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeContracts.DTO
{
    public class Cell
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public XO XO { get; set; }
    }
}
