﻿using System;
using System.Collections.Generic;
using System.Text;
using TicTacToeContracts.DTO;

namespace TicTacToeContracts.Interface
{
    public interface IBoard
    {
        bool Put(Cell cell);
        XO Get(int row, int col);

        XOStatus CheckRow(int row);

        XOStatus CheckCol(int col);
        XOStatus CheckDiagLR();
        XOStatus CheckDiagRL();


    }
}
