﻿using Contracts;
using DI;
using System;
using TicTacToeContracts.Interface;

namespace TicTacToeApp
{
    class Program
    {
        static IGame getGame()
        {
            IResolver resolver = new Resolver("dlls") ;
            //To Do Game Factory Create the entire game
            return resolver.Resolve<IGame>(); 
        }
        static void Main(string[] args)
        {
            var game = getGame();
            game.Start();//Here we will setup the player types 
            
            while (!game.EndGame())
            {
                game.P1.MakeMove(game.Board);
                if (!game.EndGame())
                {
                    game.P2.MakeMove(game.Board);
                }
                
                    

            }

        }
    }
}
