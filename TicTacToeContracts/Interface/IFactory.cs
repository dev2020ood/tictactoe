﻿using System;
using System.Collections.Generic;
using System.Text;
using TicTacToeContracts.Interface.Players;

namespace TicTacToeContracts.Interface
{
    public interface IFactory
    {
        IGame createGame();
        IPlayer createPlayer<TPlayer>() where TPlayer : IPlayer;


    }
}
